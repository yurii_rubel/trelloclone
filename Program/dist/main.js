(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Projects\trello-clone-angular\src\main.ts */"zUnb");


/***/ }),

/***/ "1zW5":
/*!********************************************!*\
  !*** ./src/app/drawer/drawer.component.ts ***!
  \********************************************/
/*! exports provided: DrawerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DrawerComponent", function() { return DrawerComponent; });
/* harmony import */ var _raw_loader_drawer_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./drawer.component.html */ "M43i");
/* harmony import */ var _drawer_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./drawer.component.scss */ "iqhz");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/cdk/layout */ "0MNC");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "9YHx");
/* harmony import */ var _board_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../board.service */ "VXsB");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var DrawerComponent = /** @class */ (function () {
    function DrawerComponent(breakpointObserver, boardService) {
        this.breakpointObserver = breakpointObserver;
        this.boardService = boardService;
        this.isHandset$ = this.breakpointObserver.observe(_angular_cdk_layout__WEBPACK_IMPORTED_MODULE_3__["Breakpoints"].Handset)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (result) { return result.matches; }));
        this.boards = boardService.getBoards();
    }
    DrawerComponent.ctorParameters = function () { return [
        { type: _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_3__["BreakpointObserver"] },
        { type: _board_service__WEBPACK_IMPORTED_MODULE_5__["BoardService"] }
    ]; };
    DrawerComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-drawer',
            template: _raw_loader_drawer_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_drawer_component_scss__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [_angular_cdk_layout__WEBPACK_IMPORTED_MODULE_3__["BreakpointObserver"],
            _board_service__WEBPACK_IMPORTED_MODULE_5__["BoardService"]])
    ], DrawerComponent);
    return DrawerComponent;
}());



/***/ }),

/***/ "2FlG":
/*!*****************************************************************************************!*\
  !*** ./src/app/shared/components/color-picker-dialog/color-picker-dialog.component.css ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9jb21wb25lbnRzL2NvbG9yLXBpY2tlci1kaWFsb2cvY29sb3ItcGlja2VyLWRpYWxvZy5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "6bmo":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/delete-talk/delete-talk.component.html ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("Are you sure that you want to delete \"{{talk.text}}\"?\n\n<mat-dialog-actions align=\"end\">\n  <button mat-button [mat-dialog-close]=\"false\">Cancel</button>\n  <button cdkFocusInitial mat-button [mat-dialog-close]=\"true\">Delete</button>\n</mat-dialog-actions>\n");

/***/ }),

/***/ "9eS5":
/*!********************************************************!*\
  !*** ./src/app/delete-talk/delete-talk.component.scss ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".mat-button {\n  text-transform: uppercase;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGVsZXRlLXRhbGsvZGVsZXRlLXRhbGsuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSx5QkFBQTtBQUNGIiwiZmlsZSI6InNyYy9hcHAvZGVsZXRlLXRhbGsvZGVsZXRlLXRhbGsuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWF0LWJ1dHRvbiB7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG59XG4iXX0= */");

/***/ }),

/***/ "AytR":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "BhP2":
/*!******************************************!*\
  !*** ./src/app/card/card.component.scss ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host {\n  display: block;\n  margin-bottom: 8px;\n}\n\n.mat-card-content {\n  margin-bottom: 0;\n}\n\n.mat-card-actions {\n  display: flex;\n  align-items: center;\n  justify-content: flex-end;\n}\n\n.mat-chip-list {\n  display: block;\n  margin-top: 12px;\n}\n\n.mat-card-image {\n  -o-object-fit: cover;\n     object-fit: cover;\n  -o-object-position: top;\n     object-position: top;\n  max-height: 150px;\n}\n\n.card {\n  position: relative;\n}\n\n.icon {\n  font-size: 17px;\n  cursor: pointer;\n  position: absolute;\n  right: -6px;\n  top: 0px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2FyZC9jYXJkLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0UsY0FBQTtFQUNBLGtCQUFBO0FBQUY7O0FBSUE7RUFDRSxnQkFBQTtBQURGOztBQUtBO0VBQ0UsYUFBQTtFQUNBLG1CQUFBO0VBQ0EseUJBQUE7QUFGRjs7QUFNQTtFQUNFLGNBQUE7RUFDQSxnQkFBQTtBQUhGOztBQU9BO0VBQ0Usb0JBQUE7S0FBQSxpQkFBQTtFQUNBLHVCQUFBO0tBQUEsb0JBQUE7RUFDQSxpQkFBQTtBQUpGOztBQWFBO0VBQ0Usa0JBQUE7QUFWRjs7QUFhQTtFQUNFLGVBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsUUFBQTtBQVZGIiwiZmlsZSI6InNyYy9hcHAvY2FyZC9jYXJkLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gT3VyIGN1c3RvbSBlbGVtZW50IHdpbGwgYmUgYGRpc3BsYXk6IGlubGluZWAgYnkgZGVmYXVsdC4gTWFrZSBpdCBhIGJsb2NrIGFuZCBhZGQgc29tZSBzcGFjaW5nLlxuOmhvc3Qge1xuICBkaXNwbGF5OiBibG9jaztcbiAgbWFyZ2luLWJvdHRvbTogOHB4O1xufVxuXG4vLyBSZW1vdmUgTWF0ZXJpYWwncyBtYXJnaW4gZnJvbSB0aGUgY29udGVudCBzaW5jZSB3ZSBoYXZlIGEgcm93IG9mIGNoaXBzLlxuLm1hdC1jYXJkLWNvbnRlbnQge1xuICBtYXJnaW4tYm90dG9tOiAwO1xufVxuXG4vLyBTcHJlYWQgb3V0IHRoZSBhY3Rpb25zIGNvbnRlbnQgdG8gdGhlIGxlZnQgYW5kIHJpZ2h0LlxuLm1hdC1jYXJkLWFjdGlvbnMge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xufVxuXG4vLyBBZGQgc29tZSBzcGFjaW5nIHRvIHRoZSBjaGlwIGxpc3Rcbi5tYXQtY2hpcC1saXN0IHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIG1hcmdpbi10b3A6IDEycHg7XG59XG5cbi8vIEFsbG93cyBmb3IgdGhlIGltYWdlIHRvIGNvdmVyIHRoZSBjYXJkIHdoaWxlIHByZXNlcnZpbmcgaXRzIGRpbWVuc2lvbnMuXG4ubWF0LWNhcmQtaW1hZ2Uge1xuICBvYmplY3QtZml0OiBjb3ZlcjtcbiAgb2JqZWN0LXBvc2l0aW9uOiB0b3A7XG4gIG1heC1oZWlnaHQ6IDE1MHB4O1xufVxuXG4vLyBJbmNyZWFzZSB0aGUgc2l6ZSBvZiB0aGUgYXV0aG9yJ3MgbmFtZSBhbmQgYWRkIHNvbWUgc3BhY2luZy5cbi8vIC5hdXRob3Ige1xuLy8gICBmb250LXNpemU6IDE0cHg7XG4vLyAgIG1hcmdpbi1sZWZ0OiA4cHg7XG4vLyB9XG5cbi5jYXJkIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4uaWNvbiB7XG4gIGZvbnQtc2l6ZTogMTdweDtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiAtNnB4O1xuICB0b3A6IDBweDtcbn1cbiJdfQ== */");

/***/ }),

/***/ "Gz50":
/*!***************************!*\
  !*** ./src/app/data.json ***!
  \***************************/
/*! exports provided: 0, default */
/***/ (function(module) {

module.exports = JSON.parse("[{\"title\":\"Day one\",\"tracks\":[{\"id\":\"track-one\",\"title\":\"Track one\",\"talks\":[{\"issueType\":\"epic\",\"text\":\"Keynote addess\",\"speaker\":\"Igor Minar\",\"createdAt\":\"2018-01-01T09:26:05.026Z\",\"image\":\"https://i.ytimg.com/vi/7Bj4R7lGl4A/maxresdefault.jpg\"},{\"issueType\":\"bug\",\"text\":\"VS Code Can Do That\",\"speaker\":\"John Papa\",\"createdAt\":\"2020-02-01T09:26:05.026Z\",\"tags\":[{\"name\":\"Intro\",\"color\":\"#e0e0e0\"}]},{\"issueType\":\"sub-task\",\"text\":\"How to save time & money by planning your ngUpgrade\",\"speaker\":\"Sam Julien\",\"createdAt\":\"2019-03-01T09:26:05.026Z\"},{\"issueType\":\"task\",\"text\":\"How to AI in JS?\",\"speaker\":\"Asim Hussain\",\"createdAt\":\"2020-04-01T09:26:05.026Z\",\"tags\":[{\"name\":\"Intro\",\"color\":\"#e0e0e0\"}]},{\"issueType\":\"story\",\"text\":\"You Might Not Need NgRx\",\"speaker\":\"Michael Ryan\",\"createdAt\":\"2020-05-01T09:26:05.026Z\"},{\"issueType\":\"story\",\"text\":\"Upgrading to Angular without ngUpgrade\",\"speaker\":\"Erin Coughlan\",\"createdAt\":\"2020-06-01T09:26:05.026Z\"},{\"issueType\":\"task\",\"text\":\"Why you need a build system, and why it should be Bazel\",\"speaker\":\"Martin Probst\",\"createdAt\":\"2020-07-01T09:26:05.026Z\"},{\"issueType\":\"task\",\"text\":\"The theory of Angular Ivy\",\"speaker\":\"Alex Rickabaugh\",\"createdAt\":\"2020-08-01T09:26:05.026Z\"},{\"issueType\":\"task\",\"text\":\"Building an Angular PWA: Angular Service Worker or Workbox?\",\"speaker\":\"Maxim Salnikov\",\"createdAt\":\"2020-09-01T09:26:05.026Z\",\"tags\":[{\"name\":\"Deep-dive\",\"color\":\"#e0e0e0\"}]},{\"issueType\":\"task\",\"text\":\"Angular Unit Testing - how to win friends, design better code, and get rich quick!\",\"speaker\":\"Shai Reznik\",\"createdAt\":\"2020-10-01T09:26:05.026Z\"}]},{\"id\":\"track-two\",\"title\":\"Track two\",\"talks\":[{\"issueType\":\"bug\",\"text\":\"Automating UI development\",\"speaker\":\"Stefan Baumgartner and Katrin Freihofner\",\"createdAt\":\"2020-11-01T09:26:05.026Z\"},{\"issueType\":\"epic\",\"text\":\"RxJS schedulers in depth\",\"image\":\"https://upload.wikimedia.org/wikipedia/commons/thumb/6/6e/London_Thames_Sunset_panorama_-_Feb_2008.jpg/640px-London_Thames_Sunset_panorama_-_Feb_2008.jpg\",\"speaker\":\"Michael Hladky\",\"createdAt\":\"2020-12-01T09:26:05.026Z\",\"tags\":[{\"name\":\"Deep-dive\",\"color\":\"#e0e0e0\"}]},{\"issueType\":\"sub-task\",\"text\":\"The good, the bad and the ugly - Component architecture at scale\",\"speaker\":\"Ana Cidre and Sherry List\",\"createdAt\":\"2019-01-01T09:26:05.026Z\",\"tags\":[{\"name\":\"Deep-dive\",\"color\":\"#e0e0e0\"}]},{\"issueType\":\"story\",\"text\":\"Universally speaking\",\"speaker\":\"Craig Spence\",\"createdAt\":\"2019-02-01T09:26:05.026Z\",\"tags\":[{\"name\":\"Deep-dive\",\"color\":\"#e0e0e0\"}]},{\"issueType\":\"task\",\"text\":\"Optimize and debug change detection like a pro\",\"speaker\":\"Max Koretskyi\",\"createdAt\":\"2019-03-01T09:26:05.026Z\",\"tags\":[{\"name\":\"Deep-dive\",\"color\":\"#e0e0e0\"}]},{\"issueType\":\"task\",\"text\":\"Nest the backend for your Angular application\",\"speaker\":\"Bo Vandersteene\",\"createdAt\":\"2019-04-01T09:26:05.026Z\",\"tags\":[{\"name\":\"Intro\",\"color\":\"#e0e0e0\"}]},{\"issueType\":\"task\",\"text\":\"Testing Angular with Cypress.io\",\"speaker\":\"Joe Eames\",\"createdAt\":\"2019-05-01T09:26:05.026Z\",\"tags\":[{\"name\":\"Intro\",\"color\":\"#e0e0e0\"}]},{\"issueType\":\"task\",\"text\":\"Location based immersive experiences with Angular and WebXR\",\"speaker\":\"Aysegul Yonet\",\"createdAt\":\"2019-06-01T09:26:05.026Z\"}]},{\"id\":\"mini-workshops\",\"title\":\"Mini workshops\",\"talks\":[{\"issueType\":\"task\",\"text\":\"Deep dive into Angular CLI 7\",\"speaker\":\"Noël Macé\",\"createdAt\":\"2019-07-01T09:26:05.026Z\"},{\"issueType\":\"task\",\"text\":\"Angular Material workshop\",\"speaker\":\"Kristiyan Kostadinov and Paul Gschwendtner\",\"createdAt\":\"2019-08-01T09:26:05.026Z\",\"image\":\"https://pbs.twimg.com/media/DoRVQNuXUAAg7Zt.jpg\"},{\"issueType\":\"task\",\"text\":\"#UseThePlatform with Stencil and Angular\",\"speaker\":\"Gil Fink\",\"createdAt\":\"2019-09-01T09:26:05.026Z\"}]},{\"id\":\"office-hours\",\"title\":\"Office hours\",\"talks\":[{\"issueType\":\"task\",\"text\":\"Stefan Baumgartner, Michael Hladky, Katrin Freihofner, Igor Minar\",\"createdAt\":\"2019-10-01T09:26:05.026Z\"},{\"issueType\":\"task\",\"text\":\"Ana Cidre, Sam Julien, Sherry List, Noël Macé, Michael Ryan\",\"image\":\"https://upload.wikimedia.org/wikipedia/commons/thumb/8/87/Palace_of_Westminster_from_the_dome_on_Methodist_Central_Hall.jpg/640px-Palace_of_Westminster_from_the_dome_on_Methodist_Central_Hall.jpg\",\"createdAt\":\"2019-10-01T09:26:05.026Z\"},{\"issueType\":\"task\",\"text\":\"Sani Yusuf, Craig Spence, Maxim Salnikov, Olivier Combe, Misko Hevery\",\"createdAt\":\"2019-11-01T09:26:05.026Z\"},{\"issueType\":\"task\",\"text\":\"Erin Coughlan, Martin Probst, Max Koretskyi, Bo Vandersteene\",\"createdAt\":\"2019-12-01T09:26:05.026Z\"}]},{\"id\":\"qa-panels\",\"title\":\"Q&A panels\",\"talks\":[{\"issueType\":\"task\",\"text\":\"Upgrade\",\"speaker\":\"Elana Olson, Erin Coughlan, Sam Julien\",\"createdAt\":\"2019-01-01T09:26:05.026Z\"},{\"issueType\":\"task\",\"text\":\"UI\",\"speaker\":\"Shmuela Jacobs, Ana Cidre, Sherry List, Kristiyan Kostadinov, Paul Gschwendtner, Katrin Freihofner\",\"createdAt\":\"2019-02-01T09:26:05.026Z\"},{\"issueType\":\"task\",\"text\":\"Ivy\",\"speaker\":\"Alex Rickabaugh, Jason Aden, Olivier Combe, Igor Minar, Misko Hevery\",\"createdAt\":\"2019-03-01T09:26:05.026Z\",\"image\":\"https://pbs.twimg.com/media/DoHCNnlUUAUWfhG.jpg\"}]}]}]");

/***/ }),

/***/ "I0oU":
/*!****************************************!*\
  !*** ./src/app/shared/appConstants.ts ***!
  \****************************************/
/*! exports provided: appConstants */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "appConstants", function() { return appConstants; });
/* harmony import */ var _models_schema_model__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./models/schema.model */ "bkVp");
var _a;

var appConstants = {
    /** Issue Types with ttheir hardcoded colors */
    issueTypeListWithColor: (_a = {},
        _a[_models_schema_model__WEBPACK_IMPORTED_MODULE_0__["IssueType"].Bug] = {
            name: _models_schema_model__WEBPACK_IMPORTED_MODULE_0__["IssueType"].Bug,
            color: '#99333352'
        },
        _a[_models_schema_model__WEBPACK_IMPORTED_MODULE_0__["IssueType"].Epic] = {
            name: _models_schema_model__WEBPACK_IMPORTED_MODULE_0__["IssueType"].Epic,
            color: '#33996652'
        },
        _a[_models_schema_model__WEBPACK_IMPORTED_MODULE_0__["IssueType"].Story] = {
            name: _models_schema_model__WEBPACK_IMPORTED_MODULE_0__["IssueType"].Story,
            color: '#fff3d4'
        },
        _a[_models_schema_model__WEBPACK_IMPORTED_MODULE_0__["IssueType"].Task] = {
            name: _models_schema_model__WEBPACK_IMPORTED_MODULE_0__["IssueType"].Task,
            color: '#99ccff61'
        },
        _a[_models_schema_model__WEBPACK_IMPORTED_MODULE_0__["IssueType"].SubTask] = {
            name: _models_schema_model__WEBPACK_IMPORTED_MODULE_0__["IssueType"].SubTask,
            color: '#3d7e9a4d'
        },
        _a)
};


/***/ }),

/***/ "LrV0":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/edit-talk/edit-talk.component.html ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<form [formGroup]=\"formGroup\" (ngSubmit)=\"onSubmit()\">\n  <img *ngIf=\"data?.talk.image\" [src]=\"data?.talk.image\" alt=\"Talk image preview\">\n  <mat-form-field>\n    <input matInput placeholder=\"Talk description\" required formControlName=\"text\">\n  </mat-form-field>\n  <mat-form-field>\n    <mat-label>Issue Type</mat-label>\n    <mat-select formControlName=\"issueType\">\n      <mat-option [style.background]=\"i.color\" *ngFor=\"let i of issueTypesArrayWithColor\" [value]=\"i.name\">\n        {{i.name}}\n      </mat-option>\n    </mat-select>\n  </mat-form-field>\n  <mat-form-field>\n    <input matInput placeholder=\"Speaker\" required formControlName=\"speaker\">\n  </mat-form-field>\n  <mat-form-field>\n    <input matInput placeholder=\"Image URL\" formControlName=\"image\">\n  </mat-form-field>\n  <mat-form-field>\n    <!-- Add the chip list and associate it with a new form control. -->\n    <mat-chip-list #tags formControlName=\"tags\">\n      <!--\n          Render out all the chips that are set to the form control.\n          Also listen for the `removed` event which will notify use when to remove a tag.\n        -->\n      <mat-chip [style.background]=\"tag?.color ? tag.color : '#e0e0e0'\" *ngFor=\"let tag of this.formGroup.get('tags').value\" (removed)=\"removeTag(tag)\">\n        {{tag?.name}}\n        <!-- Add an icon that allows the user to delete a talk via click. -->\n        <mat-icon (click)=\"openColorPickerDialog(tag)\" class=\"mat-icon notranslate mat-chip-remove mat-chip-trailing-icon material-icons mat-icon-no-color\">color_lense</mat-icon>\n        <mat-icon matChipRemove>cancel</mat-icon>\n      </mat-chip>\n\n      <!--\n          Input into which the user can type new tags. The `matChipInputTokenEnd` event will fire\n          whenever the user presses enter or they focus outside the input.\n        -->\n      <input placeholder=\"Add tags\" [matChipInputFor]=\"tags\" (matChipInputTokenEnd)=\"addTag($event)\" />\n    </mat-chip-list>\n  </mat-form-field>\n  <mat-dialog-actions align=\"end\">\n    <!--\n        mat-dialog-close is a directive from the `MatDialogModule` which can be used\n        to close the current dialog in a declarative way on element click.\n      -->\n    <button mat-button mat-dialog-close>Cancel</button>\n\n    <button mat-button type=\"submit\" [disabled]=\"formGroup.invalid\">{{data?.edit ? 'Update' : 'Add'}}</button>\n  </mat-dialog-actions>\n</form>\n");

/***/ }),

/***/ "M43i":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/drawer/drawer.component.html ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<mat-sidenav-container class=\"sidenav-container\">\n  <mat-sidenav #drawer class=\"sidenav\" fixedInViewport=\"true\"\n      [attr.role]=\"(isHandset$ | async) ? 'dialog' : 'navigation'\"\n      [mode]=\"(isHandset$ | async) ? 'over' : 'side'\"\n      [opened]=\"!(isHandset$ | async) === false\">\n    <mat-toolbar>Menu</mat-toolbar>\n    <mat-nav-list>\n      <a mat-list-item>Boards</a>\n    </mat-nav-list>\n  </mat-sidenav>\n  <mat-sidenav-content>\n    <mat-toolbar color=\"primary\">\n      <button\n        type=\"button\"\n        aria-label=\"Toggle sidenav\"\n        mat-icon-button\n        (click)=\"drawer.toggle()\">\n        <mat-icon aria-label=\"Side nav toggle icon\">menu</mat-icon>\n      </button>\n      <span>trello-clone-angular</span>\n    </mat-toolbar>\n    <!-- Add Content Here -->\n    <ng-content></ng-content>\n  </mat-sidenav-content>\n</mat-sidenav-container>\n");

/***/ }),

/***/ "QVRZ":
/*!****************************************************!*\
  !*** ./src/app/edit-talk/edit-talk.component.scss ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("img {\n  width: 500px;\n  margin: -24px -24px 0px;\n  padding-bottom: 8px;\n}\n\n.mat-form-field {\n  display: block;\n}\n\n.mat-button {\n  text-transform: uppercase;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZWRpdC10YWxrL2VkaXQtdGFsay5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFHQTtFQUNFLFlBSHVCO0VBT3ZCLHVCQUFBO0VBQ0EsbUJBQUE7QUFMRjs7QUFRQTtFQUNFLGNBQUE7QUFMRjs7QUFRQTtFQUNFLHlCQUFBO0FBTEYiLCJmaWxlIjoic3JjL2FwcC9lZGl0LXRhbGsvZWRpdC10YWxrLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiJGRpYWxvZy1kZWZhdWx0LXBhZGRpbmc6IDI0cHg7XG4kZWRpdC10YWxrLWRpYWxvZy13aWR0aDogNTAwcHg7XG5cbmltZyB7XG4gIHdpZHRoOiAkZWRpdC10YWxrLWRpYWxvZy13aWR0aDtcblxuICAvLyBTaGlmdCB0aGUgaW1hZ2UgdG8gaWdub3JlIHRoZSBkZWZhdWx0IG1hcmdpbiBvZiB0aGUgZGlhbG9nLiBXZSB3YW50IHRvXG4gIC8vIHNob3cgdGhlIHRhbGsgaW1hZ2UgYXMgYSBmdWxsLXdpZHRoIGRpYWxvZyBoZWFkZXIuXG4gIG1hcmdpbjogKC0kZGlhbG9nLWRlZmF1bHQtcGFkZGluZykgKC0kZGlhbG9nLWRlZmF1bHQtcGFkZGluZykgMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogOHB4O1xufVxuXG4ubWF0LWZvcm0tZmllbGQge1xuICBkaXNwbGF5OiBibG9jaztcbn1cblxuLm1hdC1idXR0b24ge1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xufVxuIl19 */");

/***/ }),

/***/ "Sy1n":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _raw_loader_app_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./app.component.html */ "VzVu");
/* harmony import */ var _app_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app.component.scss */ "ynWL");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _board_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./board.service */ "VXsB");
/* harmony import */ var _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/cdk/drag-drop */ "5+WD");
/* harmony import */ var _edit_talk_edit_talk_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./edit-talk/edit-talk.component */ "XE0M");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/dialog */ "0IaG");
/* harmony import */ var _delete_talk_delete_talk_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./delete-talk/delete-talk.component */ "e8aa");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __spreadArrays = (undefined && undefined.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};








var AppComponent = /** @class */ (function () {
    function AppComponent(_boardService, _dialog) {
        this._boardService = _boardService;
        this._dialog = _dialog;
        this.boards = [];
        this.boards = this._boardService.getBoards();
    }
    /**
     * An array of all track ids. Each id is associated with a `cdkDropList` for the
     * track talks. This property can be used to connect all drop lists together.
     */
    AppComponent.prototype.trackIds = function (boardIndex) {
        return this.boards[boardIndex].tracks.map(function (track) { return track.id; });
    };
    AppComponent.prototype.onTalkDrop = function (event) {
        // In case the destination container is different from the previous container, we
        // need to transfer the given talk to the target data array. This happens if
        // a talk has been dropped on a different track.
        if (event.previousContainer === event.container) {
            Object(_angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_4__["moveItemInArray"])(event.container.data, event.previousIndex, event.currentIndex);
        }
        else {
            Object(_angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_4__["transferArrayItem"])(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
        }
    };
    AppComponent.prototype.onTrackDrop = function (event) {
        Object(_angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_4__["moveItemInArray"])(event.container.data, event.previousIndex, event.currentIndex);
    };
    AppComponent.prototype.addEditTalk = function (talk, track, edit) {
        if (edit === void 0) { edit = false; }
        // Use the injected dialog service to launch the previously created edit-talk
        // component. Once the dialog closes, we assign the updated talk data to
        // the specified talk.
        this._dialog.open(_edit_talk_edit_talk_component__WEBPACK_IMPORTED_MODULE_5__["EditTalkComponent"], { data: { talk: talk, edit: edit }, width: '500px' })
            .afterClosed()
            .subscribe(function (newTalkData) { return edit ? Object.assign(talk, newTalkData) : track.talks.unshift(newTalkData); });
    };
    AppComponent.prototype.deleteTalk = function (talk, track) {
        // Open a dialog
        this._dialog.open(_delete_talk_delete_talk_component__WEBPACK_IMPORTED_MODULE_7__["DeleteTalkComponent"], { data: talk, width: '500px' })
            .afterClosed()
            .subscribe(function (response) {
            // Wait for it to close and delete the talk if the user agreed.
            if (response) {
                track.talks.splice(track.talks.indexOf(talk), 1);
            }
        });
    };
    AppComponent.prototype.filterByDate = function (talks, asc) {
        if (asc === void 0) { asc = 1; }
        talks = __spreadArrays(talks.sort(function (a, b) { return (asc) * (new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime()); }));
        console.log(talks);
    };
    AppComponent.ctorParameters = function () { return [
        { type: _board_service__WEBPACK_IMPORTED_MODULE_3__["BoardService"] },
        { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__["MatDialog"] }
    ]; };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-root',
            template: _raw_loader_app_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_app_component_scss__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [_board_service__WEBPACK_IMPORTED_MODULE_3__["BoardService"], _angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__["MatDialog"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "VXsB":
/*!**********************************!*\
  !*** ./src/app/board.service.ts ***!
  \**********************************/
/*! exports provided: BoardService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BoardService", function() { return BoardService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var BoardService = /** @class */ (function () {
    function BoardService() {
        this._boards = __webpack_require__(/*! ./data.json */ "Gz50");
    }
    BoardService.prototype.getBoards = function () {
        return this._boards;
    };
    BoardService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        })
    ], BoardService);
    return BoardService;
}());



/***/ }),

/***/ "VzVu":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-drawer>\n    <div *ngFor=\"let board of boards; let boardIndex = index;\" class=\"board\" cdkDropList [cdkDropListData]=\"board.tracks\" (cdkDropListDropped)=\"onTrackDrop($event)\"\n    cdkDropListOrientation=\"horizontal\">\n\n    <div cdkDrag class=\"card-list mat-elevation-z1\" *ngFor=\"let track of board.tracks; let trackIndex = index;\">\n      <div class=\"row\" fxLayoutAlign=\"start center\">\n        <h2 fxFlex=\"80\" class=\"mat-h2\"><span contenteditable [textContent]=\"track?.title\" (input)=\"track.title=$event.target.textContent\"></span></h2>\n        <div fxFlex=\"20\" fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"5px\" cdkDragHandle>\n          <!-- Filter Menu Starts Here-->\n          <button fxFlex=\"50\" mat-icon-button [matMenuTriggerFor]=\"menu\" aria-label=\"Example icon-button with a menu\">\n            <mat-icon>filter_list</mat-icon>\n          </button>\n          <mat-menu #menu=\"matMenu\">\n            <button mat-menu-item (click)=\"filterByDate(track.talks, -1)\">\n              <mat-icon>arrow_downward</mat-icon>\n              <span>Sort By(Date)</span>\n            </button>\n            <button mat-menu-item (click)=\"filterByDate(track.talks, 1)\">\n              <mat-icon>arrow_upward</mat-icon>\n              <span>Sort By(Date)</span>\n            </button>\n          </mat-menu>\n          <button fxFlex=\"50\" mat-icon-button aria-label=\"Example icon-button with a menu\">\n            <mat-icon>drag_handle</mat-icon>\n          </button>\n        </div>\n      </div>\n\n      <!-- Filter Menu Ends Here-->\n\n      <div *ngIf=\"!track['collapsed']\" class=\"card-list-content\" cdkDropList [id]=\"track.id\" [cdkDropListData]=\"track.talks\"\n        [cdkDropListConnectedTo]=\"trackIds(boardIndex)\" (cdkDropListDropped)=\"onTalkDrop($event)\">\n\n        <app-card *ngFor=\"let talk of track.talks\" [text]=\"talk.text\" [createdAt]=\"talk?.createdAt\" [tags]=\"talk.tags\" [issueType]=\"talk?.issueType\" [author]=\"talk.speaker\"\n          [image]=\"talk.image\" cdkDrag (edit)=\"addEditTalk(talk, track, true)\" (delete)=\"deleteTalk(talk, track)\"></app-card>\n\n      </div>\n\n      <div fxLayout=\"row\" fxLayoutAlign=\"baseline baseline\">\n        <button style=\"width: fit-content;\" class=\"add-talk\" (click)=\"addEditTalk(track, track)\" color=\"primary\" mat-raised-button>Add talk</button>\n        <span fxFlex style=\"text-align: right; cursor: pointer;\" (click)=\"track['collapsed'] = !track['collapsed']\">Click to {{ track['collapsed'] ? 'expand' : 'collapse'}}</span>\n      </div>\n    </div>\n\n  </div>\n</app-drawer>\n\n<!-- <router-outlet></router-outlet> -->");

/***/ }),

/***/ "WpTA":
/*!********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/card/card.component.html ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<mat-card class=\"card\" [style.backgroundColor]=\"issueTypesWithColor[issueType]?.color\">\n  <!-- mat-card allows us to optionally provide a card image. -->\n  <img *ngIf=\"image\" mat-card-image [src]=\"image\" draggable=\"false\">\n\n  <!-- Use the mat-card-content to add the proper spacing. -->\n  <mat-card-content>\n      {{text}}\n      <div *ngIf=\"author\">By: {{author}}</div>\n\n    <div *ngIf=\"tags\">\n      <!--\n        Render out the tags as a list of chips. Note that we want\n        readonly chips so we `selectable` to `false`.\n      -->\n      <mat-chip-list>\n        <mat-chip [style.background]=\"tag?.color ? tag.color : '#e0e0e0'\" *ngFor=\"let tag of tags\" selectable=\"false\">{{tag?.name}}</mat-chip>\n      </mat-chip-list>\n    </div>\n\n    <mat-icon (click)=\"delete.next()\" class=\"icon\">cancel_outline</mat-icon>\n  </mat-card-content>\n\n  <!-- Section for buttons and the talk author. -->\n  <mat-card-actions>\n    <!-- <button mat-button (click)=\"delete.next()\">DELETE</button> -->\n    <button mat-button (click)=\"edit.next()\">EDIT</button>\n  </mat-card-actions>\n</mat-card>\n");

/***/ }),

/***/ "XE0M":
/*!**************************************************!*\
  !*** ./src/app/edit-talk/edit-talk.component.ts ***!
  \**************************************************/
/*! exports provided: EditTalkComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditTalkComponent", function() { return EditTalkComponent; });
/* harmony import */ var _raw_loader_edit_talk_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./edit-talk.component.html */ "LrV0");
/* harmony import */ var _edit_talk_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./edit-talk.component.scss */ "QVRZ");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/dialog */ "0IaG");
/* harmony import */ var _shared_components_color_picker_dialog_color_picker_dialog_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../shared/components/color-picker-dialog/color-picker-dialog.component */ "fTly");
/* harmony import */ var _shared_appConstants__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../shared/appConstants */ "I0oU");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import { MAT_DIALOG_DATA, MatDialogRef, MatChipInputEvent } from '@angular/material';




var EditTalkComponent = /** @class */ (function () {
    function EditTalkComponent(data, dialogRef, formBuilder, colorPickerdialog) {
        this.data = data;
        this.dialogRef = dialogRef;
        this.formBuilder = formBuilder;
        this.colorPickerdialog = colorPickerdialog;
        this.issueTypesArrayWithColor = Object.values(_shared_appConstants__WEBPACK_IMPORTED_MODULE_6__["appConstants"].issueTypeListWithColor);
    }
    EditTalkComponent.prototype.ngOnInit = function () {
        var talk = this.data && this.data.talk ? this.data.talk : null;
        this.formGroup = this.formBuilder.group({
            text: [talk && talk.text ? talk.text : '', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            speaker: [talk && talk.speaker ? talk.speaker : '', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            image: [talk && talk.image ? talk.image : ''],
            tags: [talk && talk.tags ? talk.tags : []],
            issueType: [talk && talk.issueType ? talk.issueType : ''],
            createdAt: [talk && talk.createdAt ? talk.createdAt : new Date()]
        });
    };
    EditTalkComponent.prototype.onSubmit = function () {
        this.dialogRef.close(this.formGroup.value);
    };
    EditTalkComponent.prototype.removeTag = function (tag) {
        // Remove the tag from the tag control's value.
        var tagsControl = this.formGroup.get('tags');
        tagsControl.value.splice(tagsControl.value.indexOf(tag), 1);
    };
    EditTalkComponent.prototype.addTag = function (event) {
        var tagsControl = this.formGroup.get('tags');
        // Create a new array of tags, if the talk doesn't have any,
        // otherwise add the new tag to the existing array.
        if (tagsControl.value) {
            tagsControl.value.push({ name: event.value, color: '#e0e0e0' });
        }
        else {
            tagsControl.setValue([event.value]);
        }
        // Clear the input's value once the tag has been added.
        event.input.value = '';
    };
    EditTalkComponent.prototype.openColorPickerDialog = function (tag) {
        var dialogRef = this.colorPickerdialog.open(_shared_components_color_picker_dialog_color_picker_dialog_component__WEBPACK_IMPORTED_MODULE_5__["ColorPickerDialogComponent"], {
            // width: '250px',
            data: {},
            disableClose: true
        });
        dialogRef.afterClosed().subscribe(function (result) {
            console.log(result);
            if (result) {
                tag.color = result;
            }
        });
    };
    EditTalkComponent.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Inject"], args: [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MAT_DIALOG_DATA"],] }] },
        { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MatDialogRef"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] },
        { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MatDialog"] }
    ]; };
    EditTalkComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-edit-talk',
            template: _raw_loader_edit_talk_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_edit_talk_component_scss__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [Object, _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MatDialogRef"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
            _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MatDialog"]])
    ], EditTalkComponent);
    return EditTalkComponent;
}());



/***/ }),

/***/ "ZAI4":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/flex-layout */ "YUcS");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "vY5A");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "Sy1n");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser/animations */ "R1ws");
/* harmony import */ var _drawer_drawer_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./drawer/drawer.component */ "1zW5");
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/cdk/layout */ "0MNC");
/* harmony import */ var _card_card_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./card/card.component */ "mJ8H");
/* harmony import */ var _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/cdk/drag-drop */ "5+WD");
/* harmony import */ var _edit_talk_edit_talk_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./edit-talk/edit-talk.component */ "XE0M");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _delete_talk_delete_talk_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./delete-talk/delete-talk.component */ "e8aa");
/* harmony import */ var _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/toolbar */ "/t3+");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/button */ "bTqV");
/* harmony import */ var _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material/sidenav */ "XhcP");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/material/icon */ "NFeN");
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/material/list */ "MutI");
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/material/card */ "Wp6s");
/* harmony import */ var _angular_material_chips__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/material/chips */ "A5z7");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/material/dialog */ "0IaG");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/material/input */ "qFsG");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/material/menu */ "STbY");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var ngx_color_chrome__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ngx-color/chrome */ "eaoH");
/* harmony import */ var _shared_components_color_picker_dialog_color_picker_dialog_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./shared/components/color-picker-dialog/color-picker-dialog.component */ "fTly");
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! @angular/material/core */ "FKr1");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! @angular/material/select */ "d3UM");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
























 // <color-chrome></color-chrome>



var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
                _drawer_drawer_component__WEBPACK_IMPORTED_MODULE_6__["DrawerComponent"],
                _card_card_component__WEBPACK_IMPORTED_MODULE_8__["CardComponent"],
                _edit_talk_edit_talk_component__WEBPACK_IMPORTED_MODULE_10__["EditTalkComponent"],
                _delete_talk_delete_talk_component__WEBPACK_IMPORTED_MODULE_12__["DeleteTalkComponent"],
                _shared_components_color_picker_dialog_color_picker_dialog_component__WEBPACK_IMPORTED_MODULE_25__["ColorPickerDialogComponent"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_23__["CommonModule"],
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_flex_layout__WEBPACK_IMPORTED_MODULE_2__["FlexLayoutModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__["BrowserAnimationsModule"],
                _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_7__["LayoutModule"],
                _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_13__["MatToolbarModule"],
                _angular_material_button__WEBPACK_IMPORTED_MODULE_14__["MatButtonModule"],
                _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_15__["MatSidenavModule"],
                _angular_material_icon__WEBPACK_IMPORTED_MODULE_16__["MatIconModule"],
                _angular_material_list__WEBPACK_IMPORTED_MODULE_17__["MatListModule"],
                _angular_material_card__WEBPACK_IMPORTED_MODULE_18__["MatCardModule"],
                _angular_material_chips__WEBPACK_IMPORTED_MODULE_19__["MatChipsModule"],
                _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_9__["DragDropModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_11__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_11__["ReactiveFormsModule"],
                _angular_material_dialog__WEBPACK_IMPORTED_MODULE_20__["MatDialogModule"],
                _angular_material_input__WEBPACK_IMPORTED_MODULE_21__["MatInputModule"],
                _angular_material_button__WEBPACK_IMPORTED_MODULE_14__["MatButtonModule"],
                _angular_material_menu__WEBPACK_IMPORTED_MODULE_22__["MatMenuModule"],
                _angular_material_core__WEBPACK_IMPORTED_MODULE_26__["MatOptionModule"],
                _angular_material_select__WEBPACK_IMPORTED_MODULE_27__["MatSelectModule"],
                ngx_color_chrome__WEBPACK_IMPORTED_MODULE_24__["ColorChromeModule"]
            ],
            providers: [],
            entryComponents: [_edit_talk_edit_talk_component__WEBPACK_IMPORTED_MODULE_10__["EditTalkComponent"], _delete_talk_delete_talk_component__WEBPACK_IMPORTED_MODULE_12__["DeleteTalkComponent"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "bkVp":
/*!***********************************************!*\
  !*** ./src/app/shared/models/schema.model.ts ***!
  \***********************************************/
/*! exports provided: IssueType */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IssueType", function() { return IssueType; });
// export interface Issue {
//     name: IssueType;
//     color: string;
// }
var IssueType;
(function (IssueType) {
    IssueType["Task"] = "task";
    IssueType["SubTask"] = "sub-task";
    IssueType["Bug"] = "bug";
    IssueType["Epic"] = "epic";
    IssueType["Story"] = "story";
})(IssueType || (IssueType = {}));


/***/ }),

/***/ "crnd":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "crnd";

/***/ }),

/***/ "e8aa":
/*!******************************************************!*\
  !*** ./src/app/delete-talk/delete-talk.component.ts ***!
  \******************************************************/
/*! exports provided: DeleteTalkComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeleteTalkComponent", function() { return DeleteTalkComponent; });
/* harmony import */ var _raw_loader_delete_talk_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./delete-talk.component.html */ "6bmo");
/* harmony import */ var _delete_talk_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./delete-talk.component.scss */ "9eS5");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/dialog */ "0IaG");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import { MAT_DIALOG_DATA } from '@angular/material';

var DeleteTalkComponent = /** @class */ (function () {
    function DeleteTalkComponent(talk) {
        this.talk = talk;
    }
    DeleteTalkComponent.prototype.ngOnInit = function () {
    };
    DeleteTalkComponent.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Inject"], args: [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__["MAT_DIALOG_DATA"],] }] }
    ]; };
    DeleteTalkComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-delete-talk',
            template: _raw_loader_delete_talk_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_delete_talk_component_scss__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [Object])
    ], DeleteTalkComponent);
    return DeleteTalkComponent;
}());



/***/ }),

/***/ "fTly":
/*!****************************************************************************************!*\
  !*** ./src/app/shared/components/color-picker-dialog/color-picker-dialog.component.ts ***!
  \****************************************************************************************/
/*! exports provided: ColorPickerDialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ColorPickerDialogComponent", function() { return ColorPickerDialogComponent; });
/* harmony import */ var _raw_loader_color_picker_dialog_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./color-picker-dialog.component.html */ "uNZv");
/* harmony import */ var _color_picker_dialog_component_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./color-picker-dialog.component.css */ "2FlG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/dialog */ "0IaG");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ColorPickerDialogComponent = /** @class */ (function () {
    function ColorPickerDialogComponent(dialogRef) {
        this.dialogRef = dialogRef;
        this.selectedColor = '';
    }
    ColorPickerDialogComponent.prototype.ngOnInit = function () {
    };
    ColorPickerDialogComponent.prototype.onChangeComplete = function (c) {
        this.selectedColor = c.color.hex;
        console.log(c);
    };
    ColorPickerDialogComponent.ctorParameters = function () { return [
        { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__["MatDialogRef"] }
    ]; };
    ColorPickerDialogComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-color-picker-dialog',
            template: _raw_loader_color_picker_dialog_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_color_picker_dialog_component_css__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__["MatDialogRef"]])
    ], ColorPickerDialogComponent);
    return ColorPickerDialogComponent;
}());



/***/ }),

/***/ "iqhz":
/*!**********************************************!*\
  !*** ./src/app/drawer/drawer.component.scss ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".sidenav {\n  width: 200px;\n}\n\n.sidenav .mat-toolbar {\n  background: inherit;\n}\n\n.mat-toolbar.mat-primary {\n  position: sticky;\n  top: 0;\n  z-index: 1;\n  flex-shrink: 0;\n}\n\n.mat-sidenav-content {\n  display: flex;\n  flex-direction: column;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZHJhd2VyL2RyYXdlci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFJQTtFQUNFLFlBQUE7QUFIRjs7QUFNQTtFQUNFLG1CQUFBO0FBSEY7O0FBTUE7RUFDRSxnQkFBQTtFQUNBLE1BQUE7RUFDQSxVQUFBO0VBQ0EsY0FBQTtBQUhGOztBQU1BO0VBRUUsYUFBQTtFQUNBLHNCQUFBO0FBSkYiLCJmaWxlIjoic3JjL2FwcC9kcmF3ZXIvZHJhd2VyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnNpZGVuYXYtY29udGFpbmVyIHtcbiAgLy8gaGVpZ2h0OiAxMDAlO1xufVxuXG4uc2lkZW5hdiB7XG4gIHdpZHRoOiAyMDBweDtcbn1cblxuLnNpZGVuYXYgLm1hdC10b29sYmFyIHtcbiAgYmFja2dyb3VuZDogaW5oZXJpdDtcbn1cblxuLm1hdC10b29sYmFyLm1hdC1wcmltYXJ5IHtcbiAgcG9zaXRpb246IHN0aWNreTtcbiAgdG9wOiAwO1xuICB6LWluZGV4OiAxO1xuICBmbGV4LXNocmluazogMDtcbn1cblxuLm1hdC1zaWRlbmF2LWNvbnRlbnQge1xuICAvLyBDb250YWluIHRoZSBib2FyZCB3aXRoaW4gaXRzIGNvbnRhaW5lci5cbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbn1cbiJdfQ== */");

/***/ }),

/***/ "mJ8H":
/*!****************************************!*\
  !*** ./src/app/card/card.component.ts ***!
  \****************************************/
/*! exports provided: CardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CardComponent", function() { return CardComponent; });
/* harmony import */ var _raw_loader_card_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./card.component.html */ "WpTA");
/* harmony import */ var _card_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./card.component.scss */ "BhP2");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _shared_appConstants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../shared/appConstants */ "I0oU");
/* harmony import */ var _shared_models_schema_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../shared/models/schema.model */ "bkVp");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CardComponent = /** @class */ (function () {
    function CardComponent() {
        this.issueTypesWithColor = _shared_appConstants__WEBPACK_IMPORTED_MODULE_3__["appConstants"].issueTypeListWithColor;
        this.issueTypes = Object.values(_shared_models_schema_model__WEBPACK_IMPORTED_MODULE_4__["IssueType"]);
        this.edit = new _angular_core__WEBPACK_IMPORTED_MODULE_2__["EventEmitter"]();
        this.delete = new _angular_core__WEBPACK_IMPORTED_MODULE_2__["EventEmitter"]();
    }
    CardComponent.prototype.ngOnInit = function () {
    };
    CardComponent.ctorParameters = function () { return []; };
    CardComponent.propDecorators = {
        edit: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Output"] }],
        delete: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Output"] }],
        text: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"] }],
        author: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"] }],
        tags: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"] }],
        image: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"] }],
        issueType: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"] }],
        createdAt: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"] }]
    };
    CardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-card',
            template: _raw_loader_card_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_card_component_scss__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [])
    ], CardComponent);
    return CardComponent;
}());



/***/ }),

/***/ "uNZv":
/*!********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/color-picker-dialog/color-picker-dialog.component.html ***!
  \********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<color-chrome (onChangeComplete)=\"onChangeComplete($event)\"></color-chrome>\n<button mat-raised-button color=\"primary\" (click)=\"dialogRef.close(selectedColor)\">Done</button>\n<button mat-raised-button color=\"warn\" (click)=\"dialogRef.close()\">Cancel</button>\n");

/***/ }),

/***/ "vY5A":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "tyNb");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var routes = [];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "ynWL":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".board {\n  display: flex;\n  flex-direction: row;\n  width: 100%;\n  height: 85vh;\n  padding: 8px;\n  box-sizing: border-box;\n  margin-bottom: 10px;\n  overflow-x: auto;\n  overflow-y: hidden;\n}\n\n.card-list {\n  height: 100%;\n  width: 320px;\n  overflow: auto;\n  box-sizing: border-box;\n  margin-right: 8px;\n  flex-shrink: 0;\n  background: rgba(0, 0, 0, 0.03);\n  border-radius: 4px;\n  padding: 8px;\n}\n\n.card-list {\n  display: flex;\n  flex-direction: column;\n}\n\n.card-list h2 {\n  margin-bottom: 5px;\n}\n\n.card-list .add-talk {\n  flex-shrink: 0;\n  margin-top: 10px;\n}\n\n.card-list-content {\n  overflow: auto;\n  overflow-x: hidden;\n  flex-grow: 1;\n}\n\n.cdk-drag-animating, .cdk-drop-list-dragging .cdk-drag {\n  transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);\n}\n\n.cdk-drag-placeholder {\n  opacity: 0;\n}\n\n.cdk-drag-preview {\n  border: 2px dashed #673ab7;\n  box-sizing: border-box;\n  border-radius: 4px;\n  box-shadow: 0 5px 5px -3px rgba(0, 0, 0, 0.2), 0 8px 10px 1px rgba(0, 0, 0, 0.14), 0 3px 14px 2px rgba(0, 0, 0, 0.12);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBR0UsYUFBQTtFQUNBLG1CQUFBO0VBR0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0Esc0JBQUE7RUFFQSxtQkFBQTtFQUdBLGdCQUFBO0VBQ0Esa0JBQUE7QUFORjs7QUFTQTtFQUVFLFlBQUE7RUFDQSxZQUFBO0VBQ0EsY0FBQTtFQUNBLHNCQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0VBR0EsK0JBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUFURjs7QUFZQTtFQUVFLGFBQUE7RUFDQSxzQkFBQTtBQVZGOztBQVlFO0VBRUUsa0JBQUE7QUFYSjs7QUFjRTtFQUVFLGNBQUE7RUFDQSxnQkFBQTtBQWJKOztBQWlCQTtFQUVFLGNBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUFmRjs7QUFxQkE7RUFDRSxzREFBQTtBQWxCRjs7QUFzQkE7RUFDRSxVQUFBO0FBbkJGOztBQXNCQTtFQUNFLDBCQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtFQUNBLHFIQUFBO0FBbkJGIiwiZmlsZSI6InNyYy9hcHAvYXBwLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJvYXJkIHtcbiAgLy8gRmxleGJveCBzdHlsaW5nIHRoYXQgZGVmaW5lcyB0aGUgZGlyZWN0aW9uIGluIHdoaWNoIHRoZSBjb250ZW50IGlzIGZsb3dpbmcuXG4gIC8vIEFsc28gbWFrZXMgaXQgZWFzaWVyIHRvIHN0cmV0Y2ggdGhlIGxpc3RzIHRvIHRoZSBoZWlnaHQgb2YgdGhlIHNjcmVlbiBsYXRlciBvbi5cbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcblxuICAvLyBFbnN1cmUgdGhhdCB0aGUgYm9hcmQgY292ZXJzIHRoZSB2aWV3cG9ydCBhbmQgYWRkIHNvbWUgcGFkZGluZyB0byBtYWtlIGl0IGxvb2sgYmV0dGVyLlxuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiA4NXZoO1xuICBwYWRkaW5nOiA4cHg7XG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG5cbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcblxuICAvLyBPbmx5IGFsbG93IGhvcml6b250YWwgc2Nyb2xsaW5nLlxuICBvdmVyZmxvdy14OiBhdXRvO1xuICBvdmVyZmxvdy15OiBoaWRkZW47XG59XG5cbi5jYXJkLWxpc3Qge1xuICAvLyBTdHJldGNoIHRoZSBlbGVtZW50IHRvIHRoZSBoZWlnaHQgb2YgdGhlIGJvYXJkIGFuZCBtYWtlIGl0IHNjcm9sbGFibGUuXG4gIGhlaWdodDogMTAwJTtcbiAgd2lkdGg6IDMyMHB4O1xuICBvdmVyZmxvdzogYXV0bztcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgbWFyZ2luLXJpZ2h0OiA4cHg7XG4gIGZsZXgtc2hyaW5rOiAwO1xuXG4gIC8vIFNvbWUgZXh0cmEgc3R5bGluZyB0byBtYWtlIHRoZSBjYXJkIGxpc3QgbG9vayBiZXR0ZXIuXG4gIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC4wMyk7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgcGFkZGluZzogOHB4O1xufVxuXG4uY2FyZC1saXN0IHtcbiAgLy8gTWFrZXMgaXQgcG9zc2libGUgZm9yIHRoZSBgY2FyZC1saXN0LWNvbnRlbnQgYCB0byB0YWtlIHVwIHRoZSByZW1haW5pbmcgc3BhY2UuXG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG5cbiAgaDIge1xuICAgIC8vIEV4dHJhIHNwYWNpbmcgdG8gbWFrZSB0aGluZ3MgbG9vayBiZXR0ZXIuXG4gICAgbWFyZ2luLWJvdHRvbTogNXB4O1xuICB9XG5cbiAgLmFkZC10YWxrIHtcbiAgICAvLyBUaGUgZmxleC1zaHJpbmsgcHJldmVudHMgdGhlIGJ1dHRvbiBmcm9tIHNocmlua2luZyBpZiB0aGUgbGlzdCBjb250ZW50IGJlY29tZXMgdG9vIGxhcmdlLlxuICAgIGZsZXgtc2hyaW5rOiAwO1xuICAgIG1hcmdpbi10b3A6IDEwcHg7XG4gIH1cbn1cblxuLmNhcmQtbGlzdC1jb250ZW50IHtcbiAgLy8gTWFrZSB0aGUgbGlzdCBzY3JvbGxhYmxlIGFuZCBzdHJldGNoIGl0IHRvIHRha2UgdXAgdGhlIGF2YWlsYWJsZSBoZWlnaHQuXG4gIG92ZXJmbG93OiBhdXRvO1xuICBvdmVyZmxvdy14OiBoaWRkZW47XG4gIGZsZXgtZ3JvdzogMTtcbn1cblxuLy8gSWYgaXRlbXMgYXJlIGJlaW5nIHJlb3JkZXJlZCB0aHJvdWdoIGRyYWdnaW5nLCBhbGwgb3RoZXIgZWxlbWVudHMgc2hvdWxkXG4vLyByZW9yZGVyIHNtb290aGx5LiBBbHNvIGlmIGFuIGl0ZW0gaXMgYmVpbmcgZHJvcHBlZCwgaXQgc2hvdWxkIGFuaW1hdGVcbi8vIGludG8gaXRzIHRhcmdldCBwb3NpdGlvbi5cbi5jZGstZHJhZy1hbmltYXRpbmcsIC5jZGstZHJvcC1saXN0LWRyYWdnaW5nIC5jZGstZHJhZyB7XG4gIHRyYW5zaXRpb246IHRyYW5zZm9ybSAyNTBtcyBjdWJpYy1iZXppZXIoMCwgMCwgMC4yLCAxKTtcbn1cblxuLy8gSGlkZXMgdGhlIHBsYWNlaG9sZGVyIGlmIGFuIGVsZW1lbnQgaXMgYmVpbmcgZHJhZ2dlZC5cbi5jZGstZHJhZy1wbGFjZWhvbGRlciB7XG4gIG9wYWNpdHk6IDA7XG59XG5cbi5jZGstZHJhZy1wcmV2aWV3IHtcbiAgYm9yZGVyOiAycHggZGFzaGVkICM2NzNhYjc7XG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgYm94LXNoYWRvdzogMCA1cHggNXB4IC0zcHggcmdiYSgwLCAwLCAwLCAwLjIpLFxuICAgICAgICAgICAgICAwIDhweCAxMHB4IDFweCByZ2JhKDAsIDAsIDAsIDAuMTQpLFxuICAgICAgICAgICAgICAwIDNweCAxNHB4IDJweCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xufVxuIl19 */");

/***/ }),

/***/ "zUnb":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "a3Wg");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "ZAI4");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "AytR");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map